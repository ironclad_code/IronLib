package frc.robot.IronLib;

public class Utils {

    /**
     * Clamps a value within a certin range
     * 
     * @param val value to be clamped
     * @param min min value
     * @param max max value
     * @return
     */
    public static double Clamp(double val, double min, double max) {
        return Math.max(min, Math.min(max, val));
    }

    /**
     * remaps a value within one range of numbers to increase and decrease within another range of numbers
     * 
     * @param value value to be remapped
     * @param from1 minimum number in first range
     * @param to1 manimum number in first range
     * @param from2 minimum number in second range
     * @param to2 maximum number in second range
     * @return
     */
    public static double Remap(double value, double from1, double to1, double from2, double to2) {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}