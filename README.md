# IronLib

IronLib is a library aimed at creating small utility functions for FRC teams to use.

## install IronLib

ironlib can be installed as a Git submodule

```
$ git submodule add -b main https://gitlab.com/ironclad_code/IronLib.git src\main\java\frc\robot\ironlib
```

## updating IronLib

IronLib can be updated by using the git pull command inside the IronLib repo

## Forking IronLib

you can fork IronLib in order to freely make changes for your team, if you with to install the forked version run this command:

```
$ git submodule add -b main <your repo clone url> src\main\java\frc\robot\ironlib
```

## Uninstalling IronLib

```
$ git rm src\main\java\frc\robot\ironlib
```